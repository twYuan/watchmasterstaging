package com.noodoe.sampleapp.ui;

import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.content.Intent;

import com.noodoe.sampleapp.data.WatchMasterService;
import com.noodoe.sampleapp.data.model.User;

import io.reactivex.Observable;


public class LoginViewModel extends ViewModel {
    private WatchMasterService mRepository;

    public LoginViewModel() {
        this.mRepository = new WatchMasterService();
    }

    Observable<User> getLogin(String apiKey, String username, String password) {
        return mRepository.getLogin(apiKey, username, password);
    }

    void startUserProfile(Context context) {
        Intent intent = new Intent(context, UserProfileActivity.class);
        context.startActivity(intent);
    }

}
