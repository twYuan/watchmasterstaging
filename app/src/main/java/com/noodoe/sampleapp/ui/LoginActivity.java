package com.noodoe.sampleapp.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.noodoe.sampleapp.R;
import com.noodoe.sampleapp.common.SharePref;
import com.noodoe.sampleapp.data.model.User;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private String TAG = getClass().getSimpleName();
    private LoginViewModel viewModel;
    private EditText etAccount, etPassword;
    private ConstraintLayout cLayoutProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        viewModel = ViewModelProviders.of(this).get(LoginViewModel.class);

        findViewById(R.id.buttonLogin).setOnClickListener(this);
        etAccount = findViewById(R.id.editTextAccount);
        etPassword = findViewById(R.id.exitTextPassword);
        cLayoutProgress = findViewById(R.id.cLayoutProgress);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.buttonLogin:
                cLayoutProgress.setVisibility(View.VISIBLE);
                viewModel.getLogin("", etAccount.getText().toString(), etPassword.getText().toString())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new DisposableObserver<User>() {
                            @Override
                            public void onNext(User user) {
                                Log.d(TAG, "onNext");
                                SharePref.saveXParseSessionToken(LoginActivity.this, user.getSessionToken());
                                SharePref.saveUsername(LoginActivity.this, user.getUsername());
                                SharePref.saveObjectId(LoginActivity.this, user.getObjectId());
                                SharePref.saveEmail(LoginActivity.this, user.getEmail());
                                SharePref.saveTimezone(LoginActivity.this, user.getTimezone());
                                viewModel.startUserProfile(LoginActivity.this);
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, "onError, e = " + e.toString());
                                cLayoutProgress.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), R.string.login_fail, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onComplete() {
                                Log.d(TAG, "onComplete");
                                cLayoutProgress.setVisibility(View.GONE);
                            }
                        });
                break;
        }
    }
}
