package com.noodoe.sampleapp.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.noodoe.sampleapp.R;
import com.noodoe.sampleapp.common.SharePref;
import com.noodoe.sampleapp.data.model.DefaultResponse;
import com.noodoe.sampleapp.data.model.UserProfile;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class UserProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private String TAG = getClass().getSimpleName();
    private UserProfileViewModel viewModel;
    private NumberPicker numberPicker;
    private ConstraintLayout cLayoutProgress;

    private int mMinValue = -12;
    private int mMaxValue = 12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        viewModel = ViewModelProviders.of(this).get(UserProfileViewModel.class);
        viewModel.setTimeZone(SharePref.getInt(this, SharePref.TIMEZONE));

        ((TextView) findViewById(R.id.txUserName)).setText(SharePref.getString(this, SharePref.USERNAME));
        ((TextView) findViewById(R.id.txObjectId)).setText(SharePref.getString(this, SharePref.OBJECT_ID));
        ((TextView) findViewById(R.id.txEmail)).setText(SharePref.getString(this, SharePref.EMAIL));

        findViewById(R.id.btnReset).setOnClickListener(this);
        findViewById(R.id.btnApplyChang).setOnClickListener(this);
        cLayoutProgress = findViewById(R.id.cLayoutProgress);

        initNumberPicker();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnReset:
                initNumberPicker();
                break;
            case R.id.btnApplyChang:
                cLayoutProgress.setVisibility(View.VISIBLE);
                final UserProfile userProfile = new UserProfile();
                userProfile.setTimezone(numberPicker.getValue() + mMinValue);

                viewModel.putUser("", SharePref.getString(this, SharePref.XPARSE_SESSION_TOKEN), SharePref.getString(this, SharePref.OBJECT_ID), userProfile)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new DisposableObserver<DefaultResponse>() {
                            @Override
                            public void onNext(DefaultResponse defaultResponse) {
                                Log.d(TAG, "onNext");
                                viewModel.setTimeZone(userProfile.getTimezone());
                                SharePref.saveTimezone(UserProfileActivity.this, userProfile.getTimezone());
                                Toast.makeText(getApplicationContext(), R.string.user_profile_apply_success, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, "onError, e = " + e.toString());
                                cLayoutProgress.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), R.string.user_profile_apply_fail, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onComplete() {
                                Log.d(TAG, "onComplete");
                                cLayoutProgress.setVisibility(View.GONE);
                            }
                        });
                break;
        }
    }

    private void initNumberPicker() {
        numberPicker = findViewById(R.id.numberPicker);
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(mMaxValue - mMinValue);
        numberPicker.setValue(viewModel.getTimeZone() - mMinValue);
        numberPicker.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int index) {
                return Integer.toString(index + mMinValue);
            }
        });
    }
}
