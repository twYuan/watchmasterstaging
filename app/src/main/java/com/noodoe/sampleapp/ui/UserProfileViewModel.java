package com.noodoe.sampleapp.ui;

import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.content.Intent;

import com.noodoe.sampleapp.common.SharePref;
import com.noodoe.sampleapp.data.WatchMasterService;
import com.noodoe.sampleapp.data.model.DefaultResponse;
import com.noodoe.sampleapp.data.model.User;
import com.noodoe.sampleapp.data.model.UserProfile;

import io.reactivex.Observable;


public class UserProfileViewModel extends ViewModel {

    private WatchMasterService mRepository;
    private UserProfile mUserProfile;

    public UserProfileViewModel() {
        this.mUserProfile = new UserProfile();
        this.mRepository = new WatchMasterService();
    }

    public UserProfile getmUserProfile() {
        return mUserProfile;
    }

    void setTimeZone(int timezone){
        mUserProfile.setTimezone(timezone);
    }

    int getTimeZone(){
        return mUserProfile.getTimezone();
    }

    Observable<DefaultResponse> putUser(String apiKey, String seesionToken, String objectId, UserProfile userProfile) {
        return mRepository.putUser(apiKey, seesionToken, objectId, userProfile);
    }

}
