package com.noodoe.sampleapp.common;

import android.content.Context;
import android.content.SharedPreferences;


public class SharePref {
    public static String XPARSE_SESSION_TOKEN = "xParseSessionToken";
    public static String OBJECT_ID = "objectId";
    public static String USERNAME = "username";
    public static String EMAIL = "email";
    public static String TIMEZONE = "timeZone";

    public static SharedPreferences getDefaultSharedPreferences(Context context) {
        return context.getSharedPreferences(getDefaultSharedPreferencesName(context),
                getDefaultSharedPreferencesMode());
    }

    private static String getDefaultSharedPreferencesName(Context context) {
        return context.getPackageName() + "_preferences";
    }

    private static int getDefaultSharedPreferencesMode() {
        return Context.MODE_PRIVATE;
    }

    public static void saveString(Context context, String key, String value) {
        SharedPreferences sharedPref = getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void saveInt(Context context, String key, int value) {
        SharedPreferences sharedPref = getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static String getString(Context context, String key) {
        SharedPreferences sharedPref = getDefaultSharedPreferences(context);
        return sharedPref.getString(key, "");
    }

    public static int getInt(Context context, String key) {
        SharedPreferences sharedPref = getDefaultSharedPreferences(context);
        return sharedPref.getInt(key, 0);
    }

    public static void saveXParseSessionToken (Context context, String xParseSessionToken) {
        saveString(context, XPARSE_SESSION_TOKEN, xParseSessionToken);
    }

    public static void saveObjectId (Context context, String xParseSessionToken) {
        saveString(context, OBJECT_ID, xParseSessionToken);
    }

    public static void saveUsername (Context context, String username) {
        saveString(context, USERNAME, username);
    }

    public static void saveEmail (Context context, String email) {
        saveString(context, EMAIL, email);
    }

    public static void saveTimezone (Context context, int timezone) {
        saveInt(context, TIMEZONE, timezone);
    }
}
