package com.noodoe.sampleapp.common;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Retrofit2Utils {
    private static Retrofit2Utils retrofit2Utils;

    private static OkHttpClient.Builder okhttpBuilder;

    private static Retrofit.Builder retrofitBuilder;

    public static Retrofit2Utils getInstance() {
        if (retrofit2Utils == null) {
            synchronized (Retrofit2Utils.class) {
                if (retrofit2Utils == null) {
                    okhttpBuilder = new OkHttpClient.Builder()
                            .readTimeout(30, TimeUnit.SECONDS)
                            .writeTimeout(30, TimeUnit.SECONDS)
                            .connectTimeout(30, TimeUnit.SECONDS);
                    retrofit2Utils = new Retrofit2Utils();
                }
            }
        }
        return retrofit2Utils;
    }

    public Retrofit getRetrofit(String baseUrl) {
        retrofitBuilder = new Retrofit.Builder().addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        return retrofitBuilder.addConverterFactory(GsonConverterFactory.create()).client(okhttpBuilder.build()).baseUrl(baseUrl).build();
    }
}
