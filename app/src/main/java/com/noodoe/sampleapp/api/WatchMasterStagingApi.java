/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.noodoe.sampleapp.api;

import com.noodoe.sampleapp.data.model.DefaultResponse;
import com.noodoe.sampleapp.data.model.User;
import com.noodoe.sampleapp.data.model.UserProfile;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * REST API access points
 */
public interface WatchMasterStagingApi {
    String X_Parse_Application_Id = "X-Parse-Application-Id: vqYuKPOkLQLYHhk4QTGsGKFwATT4mBIGREI2m8eD";
    String X_Parse_REST_API_Key = "X-Parse-REST-API-Key";

    @Headers(X_Parse_Application_Id)
    @GET("login")
    Observable<User> getLogin(@Header(X_Parse_REST_API_Key) String apiKey, @Query("username") String userName, @Query("password") String passWord);

    @Headers(X_Parse_Application_Id)
    @PUT("users/{objectId}")
    Observable<DefaultResponse> putUser(@Header(X_Parse_REST_API_Key) String apiKey, @Header("X-Parse-Session-Token") String sessionToken, @Path("objectId") String objectId, @Body UserProfile userProfile);

}