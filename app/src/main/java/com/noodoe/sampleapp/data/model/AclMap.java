package com.noodoe.sampleapp.data.model;

import java.util.Map;

public class AclMap
{
    public Map<String, ACL> aclMap;

    public Map<String, ACL> getAclMap() {
        return aclMap;
    }

}