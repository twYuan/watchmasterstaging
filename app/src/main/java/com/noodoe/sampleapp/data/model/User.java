package com.noodoe.sampleapp.data.model;

public class User {
    private String updatedAt;

    private String sessionToken;

    private int timezone;

    private String username;

    private String reportEmail;

    private String email;

    private String createdAt;

    private String objectId;

    private AclMap ACL;

    private String code;

    private String reportmail;

    public String getUpdatedAt ()
    {
        return updatedAt;
    }

    public String getSessionToken ()
    {
        return sessionToken;
    }

    public int getTimezone ()
    {
        return timezone;
    }

    public String getUsername ()
    {
        return username;
    }

    public String getReportEmail ()
    {
        return reportEmail;
    }

    public String getEmail ()
    {
        return email;
    }

    public String getCreatedAt ()
    {
        return createdAt;
    }

    public String getObjectId ()
    {
        return objectId;
    }

    public AclMap getACL() {
        return ACL;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getReportmail ()
    {
        return reportmail;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [updatedAt = "+updatedAt+", sessionToken = "+sessionToken+", timezone = "+timezone+", username = "+username+", reportEmail = "+reportEmail+", email = "+email+", createdAt = "+createdAt+", objectId = "+objectId+", ACL = "+ACL+", code = "+code+", reportmail = "+reportmail+"]";
    }
}

