package com.noodoe.sampleapp.data.model;

public class Role
{
    private String __op;

    public String get__op ()
    {
        return __op;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [__op = "+__op+"]";
    }
}
