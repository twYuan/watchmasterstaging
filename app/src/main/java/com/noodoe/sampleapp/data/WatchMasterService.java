package com.noodoe.sampleapp.data;

import com.noodoe.sampleapp.api.WatchMasterStagingApi;
import com.noodoe.sampleapp.common.Retrofit2Utils;
import com.noodoe.sampleapp.data.model.DefaultResponse;
import com.noodoe.sampleapp.data.model.User;
import com.noodoe.sampleapp.data.model.UserProfile;

import io.reactivex.Observable;
import retrofit2.Retrofit;

public class WatchMasterService {
    private static String baseUrl = "https://watch-master-staging.herokuapp.com/api/";
    private WatchMasterStagingApi watchMasterStagingApi;

    public WatchMasterService() {
        Retrofit retrofit = Retrofit2Utils.getInstance().getRetrofit(baseUrl);
        this.watchMasterStagingApi = retrofit.create(WatchMasterStagingApi.class);
    }

    public Observable<User> getLogin(String apiKey, String username, String password) {
        return watchMasterStagingApi.getLogin(apiKey, username, password);
    }

    public Observable<DefaultResponse> putUser(String apiKey, String seesionToken, String objectId, UserProfile userProfile) {
        return watchMasterStagingApi.putUser(apiKey, seesionToken, objectId, userProfile);
    }
}
