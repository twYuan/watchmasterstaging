package com.noodoe.sampleapp.data.model;

public class UserProfile {
    private Integer timezone;

    public Integer getTimezone() {
        return timezone;
    }

    public void setTimezone(Integer timezone) {
        this.timezone = timezone;
    }
}
