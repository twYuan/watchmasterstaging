package com.noodoe.sampleapp.data.model;

public class ACL {
    private String write;

    private String read;

    public String getWrite ()
    {
        return write;
    }

    public String getRead ()
    {
        return read;
    }


    @Override
    public String toString()
    {
        return "ACL [write = "+write+", read = "+read+"]";
    }
}
