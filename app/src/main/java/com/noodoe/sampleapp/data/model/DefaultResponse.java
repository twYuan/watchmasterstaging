package com.noodoe.sampleapp.data.model;

public class DefaultResponse {
    private String updatedAt;

    private Role role;

    public String getUpdatedAt ()
    {
        return updatedAt;
    }

    public Role getRole ()
    {
        return role;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [updatedAt = "+updatedAt+", role = "+role+"]";
    }
}
